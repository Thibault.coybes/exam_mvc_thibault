<?php

$routes = array(
    array('home', 'default', 'index'),
    array('addUsers', 'users', 'addUsers'),
    array('addCreneaux', 'creneaux', 'addCreneaux'),
    array('addSalle', 'salle', 'addSalle'),
    array('singleCreneau', 'creneaux', 'singleCreneau', array('id')),
);
