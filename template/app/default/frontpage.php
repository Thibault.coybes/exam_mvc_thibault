<table>
    <h2>Tous les utilisateurs</h2>
    <thead>
        <tr>
            <th>Nom</th>
            <th>Email</th>
        </tr>
    </thead>
    <tbody>
        <?php

        use App\model\CreneauxModel;
        use App\model\DefaultModel;

        foreach ($allUsers as $user) {
            echo '
                <tr>
                    <th>' . $user->nom . '</th>
                    <th>' . $user->email . '</th>
                </tr>
                ';
        }
        ?>
    </tbody>
</table>
<table>
    <h2>Toutes les salles</h2>
    <thead>
        <tr>
            <th>Title</th>
            <th>Utilisateurs Max</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($allSalles as $salle) {
            echo '
                <tr>
                    <th>' . $salle->title . '</th>
                    <th>' . $salle->maxuser . '</th>
                </tr>
                ';
        }
        ?>
    </tbody>
</table>
<table>
    <h2>Tout les créneaux</h2>
    <thead>
        <tr>
            <th>Salle</th>
            <th>Début</th>
            <th>Nombre d'heure</th>
            <th>Options</th>
        </tr>
    </thead>
    <tbody>
        <?php
        foreach ($allCreneaux as $creneau) {
            echo '
                <tr>
                <th>' . DefaultModel::getSalleById($creneau->id_salle)[0]->title . '</th>
                    <th>' . $creneau->start_at . '</th>
                    <th>' . $creneau->nbrhours . '</th>
                    <th><a href="' . $view->path("singleCreneau", array('id' => $creneau->id)) . '">Voir plus</a></th>
                </tr>
                ';
        }
        ?>
    </tbody>
</table>