<div class="containerAdd">
    <?= '<h1>' . $message . '</h1>'; ?>

    <form action="" method="post">
        <?= $form->label('nom') ?>
        <?= $form->input('nom') ?>
        <?= $form->error('nom') ?>

        <?= $form->label('email') ?>
        <?= $form->input('email') ?>
        <?= $form->error('email') ?>

        <?= $form->submit('submitted') ?>
    </form>
</div>