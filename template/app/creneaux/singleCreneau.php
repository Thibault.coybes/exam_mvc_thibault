<h1><?= $salle[0]->title ?></h1>

<div class="containerAdd">
    <h2>Ajouter un utilisateurs</h2>
    <form action="" method="post">
        <?= $form->label('Choisir un Utilisateur') ?>
        <?= $form->selectEntity('users', $users, 'nom') ?>
        <?= $form->error('users') ?>

        <?= $form->submit('submitted') ?>
    </form>

    <h2>Tout les Utilisateurs Participants</h2>

    <?php
    foreach ($usersCreneau as $user) {
        echo '<p>' . $user->nom . '</p>';
    }
    ?>
</div>