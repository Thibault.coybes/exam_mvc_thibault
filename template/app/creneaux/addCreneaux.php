<div class="containerAdd">
    <?= '<h1>' . $message . '</h1>'; ?>

    <form action="" method="post">
        <?= $form->label('Nom de la Salle') ?>
        <?= $form->selectEntity('salle', $allSalle, 'title') ?>
        <?= $form->error('salle') ?>

        <?= $form->label('Nombre d\'Heure') ?>
        <?= $form->input('nbrHours') ?>
        <?= $form->error('nbrHours') ?>

        <?= $form->label('Date de début') ?>
        <?= $form->input('start_at', 'date') ?>
        <?= $form->error('start_at') ?>

        <?= $form->submit('submitted'); ?>
    </form>
</div>