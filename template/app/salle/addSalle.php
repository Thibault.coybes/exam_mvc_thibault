<div class="containerAdd">
    <?= '<h1>' . $message . '</h1>'; ?>

    <form action="" method="post">
        <?= $form->label('title') ?>
        <?= $form->input('title') ?>
        <?= $form->error('title') ?>

        <?= $form->label('Nombre d\'utilisateur maximum') ?>
        <?= $form->input('maxUsers') ?>
        <?= $form->error('maxUsers') ?>

        <?= $form->submit('submitted') ?>
    </form>
</div>