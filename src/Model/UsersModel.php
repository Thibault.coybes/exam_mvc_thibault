<?php

namespace App\model;

use Core\App;
use Core\Kernel\AbstractModel;

class UsersModel extends AbstractModel
{
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO users (nom,email) VALUES (?,?)",
            array($post['nom'], $post['email'])
        );
    }
    public static function findIdByEmail($email)
    {
        $table = 'users';
        return App::getDatabase()->prepare("SELECT * FROM " . self::$table . " WHERE email = ?", [$email], get_called_class(), true);
    }
}
