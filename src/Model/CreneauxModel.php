<?php

namespace App\model;

use Core\App;
use Core\Kernel\AbstractModel;

class CreneauxModel extends AbstractModel
{
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO creneau (id_salle,nbrhours,start_at) VALUES (?,?,?)",
            array($post['salle'], $post['nbrHours'], $post['start_at'])
        );
    }
    public static function insertUser($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO creneau_user (id_user, id_creneau, created_at) VALUES (?,?,NOW())",
            array($post['users'], $post['id_creneau'])
        );
    }
    public static function getAllSalle()
    {
        return App::getDatabase()->query("SELECT * FROM salle", get_called_class());
    }
    public static function getCreneauById($id)
    {
        return App::getDatabase()->query("SELECT * FROM creneau WHERE id = " . $id, get_called_class());
    }
    public static function getUsers()
    {
        return App::getDatabase()->query("SELECT * FROM users ", get_called_class());
    }
    public static function getUsersinCreneauById($id)
    {
        return App::getDatabase()->query("SELECT *
        FROM users
        LEFT JOIN creneau_user ON $id = users.id", get_called_class());
    }
}
