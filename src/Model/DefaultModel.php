<?php

namespace App\model;

use Core\App;
use Core\Kernel\AbstractModel;

class DefaultModel extends AbstractModel
{
    public static function getAllUser()
    {
        return App::getDatabase()->query("SELECT * FROM users", get_called_class());
    }
    public static function getAllSalle()
    {
        return App::getDatabase()->query("SELECT * FROM salle", get_called_class());
    }
    public static function getAllCreneaux()
    {
        return App::getDatabase()->query("SELECT * FROM creneau", get_called_class());
    }
    public static function getSalleById($id)
    {
        return App::getDatabase()->query("SELECT *
        FROM salle
        LEFT JOIN creneau ON $id = salle.id", get_called_class());
    }
}
