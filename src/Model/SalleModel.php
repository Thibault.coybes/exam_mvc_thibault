<?php

namespace App\model;

use Core\App;
use Core\Kernel\AbstractModel;

class SalleModel extends AbstractModel
{
    public static function insert($post)
    {
        App::getDatabase()->prepareInsert(
            "INSERT INTO salle (title,maxuser) VALUES (?,?)",
            array($post['title'], $post['maxUsers'])
        );
    }
}
