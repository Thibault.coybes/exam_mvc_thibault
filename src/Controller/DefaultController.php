<?php

namespace App\Controller;

use App\model\DefaultModel;
use Core\Kernel\AbstractController;

/**
 *
 */
class DefaultController extends AbstractController
{
    public function index()
    {
        $allUsers = DefaultModel::getAllUser();
        $allSalles = DefaultModel::getAllSalle();
        $allCreneaux = DefaultModel::getAllCreneaux();
        $this->render('app.default.frontpage', array(
            'allUsers' => $allUsers,
            'allSalles' => $allSalles,
            'allCreneaux' => $allCreneaux,
        ));
    }

    /**
     * Ne pas enlever
     */
    public function Page404()
    {
        $this->render('app.default.404');
    }
}
