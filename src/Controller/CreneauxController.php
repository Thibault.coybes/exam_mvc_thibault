<?php

namespace App\Controller;

use App\model\CreneauxModel;
use App\model\DefaultModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class CreneauxController extends AbstractController
{
    public function addCreneaux()
    {
        $message = 'Ajouter un Créneaux';
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            if ($v->IsValid($error)) {
                CreneauxModel::insert($post);
                $this->redirect('home');
            }
        }
        $form = new Form($error);
        $allSalle = CreneauxModel::getAllSalle();
        $this->render('app.creneaux.addCreneaux', array(
            'allSalle' => $allSalle,
            'form' => $form,
            'message' => $message,
        ), 'base');
    }
    public function singleCreneau($id)
    {
        $creneau = CreneauxModel::getCreneauById($id);
        $salle = DefaultModel::getSalleById($creneau[0]->id_salle);
        $users = CreneauxModel::getUsers();
        $usersCreneau = CreneauxModel::getUsersInCreneauById($creneau[0]->id);
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $post['id_creneau'] = $id;
            if (empty($post['nbrHours']) || !ctype_digit($post['nbrHours'])) {
                $error['nbrHours'] = 'Veuillez renseignez une horraire';
            }
            if ($v->IsValid($error)) {
                CreneauxModel::insertUser($post);
                $this->redirect('#');
            }
        }
        $form = new Form($error);
        $this->render('app.creneaux.singleCreneau', array(
            'creneau' => $creneau,
            'salle' => $salle,
            'form' => $form,
            'users' => $users,
            'usersCreneau' => $usersCreneau,
        ), 'base');
    }
}
