<?php

namespace App\Controller;

use App\model\CreneauxModel;
use App\model\SalleModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class SalleController extends AbstractController
{
    public function addSalle()
    {
        $message = 'Ajouter une Salle';
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $error['title'] = $v->textValid($post['title'], 'title', 2, 100);
            if (empty($post['maxUsers']) || !ctype_digit($post['maxUsers'])) {
                $error['maxUsers'] = 'Veuillez renseigner un nombre';
            }
            if ($v->IsValid($error)) {
                SalleModel::insert($post);
                $this->redirect('home');
            }
        }
        $form = new Form($error);
        $this->render('app.salle.addSalle', array(
            'form' => $form,
            'message' => $message,
        ), 'base');
    }
}
