<?php

namespace App\Controller;

use App\model\UsersModel;
use App\Service\Form;
use App\Service\Validation;
use Core\Kernel\AbstractController;

class UsersController extends AbstractController
{
    public function addUsers()
    {
        $message = 'Ajouter un Utilisateur';
        $error = [];
        if (!empty($_POST['submitted'])) {
            $post = $this->cleanXss($_POST);
            $v = new Validation();
            $error['nom'] = $v->textValid($post['nom'], 'nom', 2, 100);
            if (empty($post['email'])) {
                $error['email'] = 'Veuillez renseigner un email';
            } else {
                $error['email'] = $v->emailValid($post['email']);
                if (UsersModel::findIdByEmail($post['email'])) {
                    $error['email'] = 'Cette adresse email et déja utilisé.';
                }
            }
            if ($v->IsValid($error)) {
                UsersModel::insert($post);
                $this->redirect('home');
            }
        }
        $form = new Form($error);

        $this->render('app.users.addUsers', array(
            'form' => $form,
            'message' => $message,
        ), 'base');
    }
}
